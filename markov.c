// Daniel Wimmer
// STC Intro to Scientific Programming
// Fall 2016
// Markov-Chain Problem HW, Markov Chain Problem

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mkl.h"
#include <time.h>

#define G(i,j) G[(i) + 10*(j)]
#define GD(i,j) GD[(i) + 10*(j)]
#define uaT(i,j) uaT[(i) + 10*(j)]
#define A(i,j) A[(i) + 10*(j)]
#define eeT(i,j) eeT[(i) + 10*(j)]

int main()
{
   FILE* fp;
   fp = fopen("markovoutput.txt", "w+");
   // Build and initialize matrices as appropriate
   int i,j,highest_index,lowest_index;
   double highest, lowest;
   double p = 0.85;
   double *G = mkl_malloc(10*10*sizeof(double),64);
   double *GD = mkl_malloc(10*10*sizeof(double),64);
   double *uaT = mkl_malloc(10*10*sizeof(double),64);
   double *eeT = mkl_malloc(10*10*sizeof(double),64);
   double *A = mkl_malloc(10*10*sizeof(double),64);
   double *x = mkl_malloc(10*sizeof(double),64);
   double *a = mkl_malloc(10*sizeof(double),64);
   double *u = mkl_malloc(10*sizeof(double),64);   
   double *e = mkl_malloc(10*sizeof(double),64);
   double *col_sums = mkl_malloc(10*sizeof(double),64);
   double *y = mkl_malloc(10*sizeof(double),64);
   double *rank = mkl_malloc(10*sizeof(double),64);
   
   // G: If team i beats team j: G(i,j) = difference in score
   //    If team j beats team i: G(i,j) = 0
   double Gtemp[10][10] =
   {
      {0,0,0,0,0,0,0,0,0,21},
      {0,0,0,0,0,0,0,0,1,21},
      {0,0,0,0,0,0,0,0,36,0},
      {5,6,7,0,0,21,0,0,0,0},
      {0,0,0,0,0,0,0,11,42,3},
      {3,0,6,0,0,0,0,0,0,0},
      {0,24,31,0,0,1,0,0,0,0},
      {18,0,0,0,0,0,0,0,24,7},
      {0,0,0,0,0,0,0,0,0,0},
      {0,0,0,0,0,0,0,0,0,0}
   };

   for(i=0;i<10;i++)
   {
      for(j=0;j<10;j++)
         G(i,j) = Gtemp[i][j];
   }

   fprintf(fp,"Matrices and final results from Markov Problem (Problem1):\n\n");
   fprintf(fp,"Matrix G:\n");
   for(i=0;i<10;i++)
   {
      for(j=0;j<10;j++)
         fprintf(fp,"%.4f ",G(i,j));
      fprintf(fp,"\n");
   }

   // a = 1 for undefeated teams, 0 otherwise
   double atemp[10] = {0,0,0,1,1,0,1,0,0,0};

   for(i=0;i<10;i++)
      a[i] = atemp[i];

   fprintf(fp,"\nVector a:\n");
   for(i=0;i<10;i++)
      fprintf(fp,"%.4f ",a[i]);
   fprintf(fp,"\n");

   // u = 1/10 for all i
   for(i=0;i<10;i++)
      u[i] = 0.1;

   fprintf(fp,"\nVector u:\n");
   for(i=0;i<10;i++)
      fprintf(fp,"%.4f ",u[i]);
   fprintf(fp,"\n");

   // e = vector of all 1's
   for(i=0;i<10;i++)
      e[i] = 1;

   // Calculate column sums

   for(j=0;j<10;j++)
   {
      col_sums[j] = 0;
      for(i=0;i<10;i++)
         col_sums[j] = col_sums[j] + G(i,j);
   }

   for(j=0;j<10;j++)
   {
      if(col_sums[j] == 0)
         col_sums[j] = 1;
   }  

   // Divide each element by respective column sum to form GD

   for(i=0;i<10;i++)
   {
      for(j=0;j<10;j++)
         GD(i,j) = G(i,j) / col_sums[j];
   }

   fprintf(fp,"\nMatrix GD:\n");
   for(i=0;i<10;i++)
   {
      for(j=0;j<10;j++)
         fprintf(fp,"%.4f ",GD(i,j));
      fprintf(fp,"\n");
   }
   fprintf(fp,"\n");

   // Now, GD = GD + u*a^T
  
   for(i=0;i<10;i++)
   {
      for(j=0;j<10;j++)
         uaT(i,j) = u[i] * a[j];
   } 

   printf("\n\nu:\n");
   for(i=0;i<10;i++)
      printf("%.4f\n",u[i]);

   printf("\n\na:\n");
   for(i=0;i<10;i++)
      printf("%.4f\n",a[i]);

   printf("\n\nuaT\n");
   for(i=0;i<10;i++)
   {
      for(j=0;j<10;j++)
         printf("%.4f ",uaT(i,j));
      printf("\n");
   }      

   for(i=0;i<10;i++)
   {
      for(j=0;j<10;j++)
         GD(i,j) = GD(i,j) + uaT(i,j);
   }

   printf("\n\nG:\n");
   for(i=0;i<10;i++)
   {
      for(j=0;j<10;j++)
         printf("%.4f ",G(i,j));
      printf("\n");
   }


   printf("\n\nGD:\n");
   for(i=0;i<10;i++)
   {
      for(j=0;j<10;j++)
         printf("%.4f ",GD(i,j));
      printf("\n");
   }

   // Markov matrix, A = p(GD + u*a^T) + (1-p)(1/10)*e*e^T   
   
   for(i=0;i<10;i++)
   {
      for(j=0;j<10;j++)
         eeT(i,j) = e[i] * e[j];
   }

   for(i=0;i<10;i++)
   {
      for(j=0;j<10;j++)
         A(i,j) = p*GD(i,j) + (1-p)*(0.1);
   }

   fprintf(fp,"\nMatrix A:\n");
   for(i=0;i<10;i++)
   {
      for(j=0;j<10;j++)
         fprintf(fp,"%.4f ",A(i,j));
      fprintf(fp,"\n");
   }
   fprintf(fp,"\n");

   printf("\n\nA:\n");
   for(i=0;i<10;i++)
   {
      for(j=0;j<10;j++)
         printf("%.4f ",A(i,j));
      printf("\n");
   }

   // Initial guess for probability vector x is filled with random values between 0 and 0.8
   srand(time(NULL));

   for(i=0;i<10;i++)
   {  
      x[i] = (double)rand() / (double)RAND_MAX;
      while(x[i] > 0.8)
         x[i] = (double)rand() / (double)RAND_MAX;
   }

   fprintf(fp,"\nInitial probability vector x:\n");
   for(i=0;i<10;i++)
      fprintf(fp,"%.4f ",x[i]);
   fprintf(fp,"\n");

   printf("\n\nRandom x:\n");
   for(i=0;i<10;i++)
      printf("%.4f\n", x[i]);
   
   // Perform Ax = x 20 times using power method as x converges

   for(i=0;i<20;i++)
   {   
      cblas_dgemv(CblasRowMajor,CblasTrans,10,10,1.0,A,10,x,1,0.0,y,1);
      for(j=0;j<10;j++)
      {      
         x[j] = y[j];
         printf("%.4f ",x[j]);
      }
      printf("\n");
   }

   // Print out final x vector
   printf("\n\nx:\n");
   for(i=0;i<10;i++)
      printf("%.4f\n", y[i]);

   // Find ranking

   for(i=0;i<10;i++)
      rank[i] = x[i];

   for(j=0;j<10;j++)
   {
      highest = 0.0;
      lowest = 1.0;
      highest_index = 0;
      lowest_index = 0;
      
      for(i=0;i<10;i++)
      {
         if((x[i] > highest) && (x[i] < 1.0))
         {
            highest = x[i];
            highest_index = i;
         } 
         if((x[i] < lowest) && (x[i] < 1.0))
         {         
            lowest = x[i];
            lowest_index = i;
         }
      }
      x[highest_index] = (double)j + 1;
   }

   printf("\n\nRank:\n");
   for(i=0;i<10;i++)
      printf("%.0f\n",x[i]);
   printf("\n\n");

   fprintf(fp,"\nFinal Rankings:\n");
   for(i=0;i<10;i++)
      fprintf(fp,"Team %d: %.0f\n",i+1,x[i]);
   fprintf(fp,"\n");

   mkl_free(G);
   mkl_free(GD);
   mkl_free(uaT);
   mkl_free(eeT);
   mkl_free(A);
   mkl_free(x);
   mkl_free(a);
   mkl_free(u);
   mkl_free(e);
   mkl_free(col_sums);
   mkl_free(y);
   mkl_free(rank);


   return 0;
}
