// fDaniel Wimmer
// SDS394 Scientific and Technical Computing
// Fall 2016
// Markov Chain Homework, Part 2: Monte Carlo Problem

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{

   int i, iterations;
   double x, term1, term2, sum, pi;

   iterations = pow(10,6);

   FILE* fp;
   fp = fopen("montecarlooutput.txt", "w+");

   // Perform the equation pi = 4*integral(0,1)of (1-x^2)^0.5 dx using Monte Carlo method

   // Use 10^6 random numbers between 0 and 1 as x. Perform calculation and summation

   sum = 0.0;

   for(i=0;i<iterations;i++)
   {
      x = (double)rand() / (double)RAND_MAX;
      term1 = 1 - pow(x,2);
      term2 = pow(term1,0.5);
      sum = sum + term2;
   }

   pi = 4 * sum / iterations;

   printf("\n\nSum: %.8f\n\n", pi);
   fprintf(fp, "Output from Monte Carlo Simulation (Homework, Part2): \n");
   fprintf(fp, "Using 10^6 iterations. pi = %.8f\n", pi);
   
   fclose(fp);
   
   return 0;
}
